//
//  MovieListViewModel.swift
//  SKY
//
//  Created by Raphael Abreu on 22/02/2018.
//  Copyright © 2018 Sky Brasil. All rights reserved.
//

import Foundation

class MovieListViewModel {
    
    var movieViewModels: [MovieViewModel] = []
    
    /// Função que monta o array de movies e fornece para View
    ///
    /// - Parameter completion: closure que é executada em caso de sucesso ou falha para tratamento da View
    func requestMovies(completion: @escaping (_ success: Bool) -> Void) {
        
        RequestManager.requestMovies { (Result) in
            switch Result {
            case .success(let movies):
                for movie in movies {
                    self.movieViewModels.append(MovieViewModel(movie))
                }
                completion(true)
                break
            case .failure(_):
                completion(false)
                break
            }
        }
    }
}
