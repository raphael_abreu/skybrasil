//
//  MovieViewModel.swift
//  SKY
//
//  Created by Raphael Abreu on 22/02/2018.
//  Copyright © 2018 Sky Brasil. All rights reserved.
//

import Foundation
import UIKit

class MovieViewModel {
    
    var movie: Movie!
    var title: String? {
        get {
            return movie.title
        }
    }
    var coverUrl: URL? {
        get {
            return URL(string: movie.coverUrl!)
        }
    }
    
    init(_ movie: Movie) {
        self.movie = movie
    }
}
