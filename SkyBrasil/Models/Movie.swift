//
//  Movie.swift
//  SKY
//
//  Created by Raphael Abreu on 22/02/2018.
//  Copyright © 2018 Sky Brasil. All rights reserved.
//

import Foundation
import ObjectMapper
import UIKit


class Movie : Mappable {
    
    var id: String?
    var title: String?
    var overview: String?
    var duration: String?
    var releaseYear: String?
    var coverUrl: String?
    static let coverPlaceholder = UIImage(named: "coverPlaceholder")!
    var backdropsUrl: String?
    
    init() {
        self.id = ""
        self.title = ""
        self.overview = ""
        self.duration = ""
        self.releaseYear = ""
        self.coverUrl = ""
        self.backdropsUrl = ""
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.title <- map["title"]
        self.overview <- map["overview"]
        self.duration <- map["duration"]
        self.releaseYear <- map["release_year"]
        self.coverUrl <- map["cover_url"]
        self.backdropsUrl <- map["backdrops_url"]
    }
}
