//
//  ViewController.swift
//  SKY
//
//  Created by Raphael Abreu on 22/02/2018.
//  Copyright © 2018 Sky Brasil. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class ViewController: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var btnReload: UIButton!
    
    
    let loading: UIActivityIndicatorView = UIActivityIndicatorView.init()
    var movieListViewModel: MovieListViewModel = MovieListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerCell()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        requestMovies()
    }
    
    
    /// Função que registra a célula MovieCollectionViewCell na CollectionView
    func registerCell() {
        self.collectionView.register(MovieCollectionViewCell.nib, forCellWithReuseIdentifier: MovieCollectionViewCell.identifier)
    }
    
    
    /// Função que configura e adiciona o loading na View
    func setupLoading() {
        loading.center = collectionView.center
        loading.hidesWhenStopped = true
        if loading.superview == nil {
            self.view.addSubview(loading)
        }
        loading.startAnimating()
    }
    
    
    /// Função que exibe um alert em caso de falha na requisição dos filmes
    func showError() {
        
        let alertController = UIAlertController(title: "SKY", message: "Ocorreu um problema ao carregar os filmes!", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (action) in
            self.collectionView.isHidden = true
            self.btnReload.isHidden = false
            self.loading.stopAnimating()
        }
        alertController.addAction(cancel)
        let recarregar = UIAlertAction(title: "Recarregar", style: UIAlertActionStyle.default) { (action) in
            self.requestMovies()
        }
        alertController.addAction(recarregar)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    /// Função que cria o loading e faz a requisição dos filmes a serem exibidos na CollectionView
    func requestMovies() {
        setupLoading()
        collectionView.isHidden = true
        if self.collectionView.isHidden {
            self.collectionView.isHidden = false
        }
        self.btnReload.isHidden = true
        
        movieListViewModel.requestMovies { (success) in
            
            if success {
                self.loading.stopAnimating()
                self.collectionView.isHidden = false
                self.collectionView.reloadData()
            } else {
                self.showError()
            }
        }
    }
    
    
    /// Action do botão de realod que é exibido em caso de falha na requisição dos filmes
    @IBAction func reloadMovies(_ sender: Any) {
        requestMovies()
    }
}

extension ViewController: UICollectionViewDataSource {
    
    /// Delegate que define o numero de itens a serem exibidos na collectionView
    ///
    /// - Parameters:
    ///   - collectionView: collectionView
    ///   - section: Número da section
    /// - Returns: Inteiro com o valor da quantidade de itens a serem exibidos
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieListViewModel.movieViewModels.count
    }
    
    
    /// Delegate que retorna a célula de cada item na collectionView pelo indexPath
    ///
    /// - Parameters:
    ///   - collectionView: collectionView
    ///   - indexPath: Indice do item a ser exibido
    /// - Returns: Célula tratada para collectionView
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let movieViewModel = movieListViewModel.movieViewModels[indexPath.item]
        
        return MovieCollectionViewCell.configureCell(collectionView: collectionView, cellForItemAt: indexPath, movieViewModel: movieViewModel)
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    
    /// Delegate que define o tamanho de cada célula dentro da collectionView
    ///
    /// - Parameters:
    ///   - collectionView: collectionView
    ///   - collectionViewLayout: collectionView com propriedades e métodos referentes ao layout
    ///   - indexPath: Indice do item a ser formatado
    /// - Returns: Tamanho da celula (Largura, Altura)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width / 2) - 10, height: (collectionView.frame.size.width / 2) * 1.60)
    }
}
