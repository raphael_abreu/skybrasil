//
//  RequestManager.swift
//  SKY
//
//  Created by Raphael Abreu on 22/02/2018.
//  Copyright © 2018 Sky Brasil. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


enum Result {
    case success([Movie])
    case failure(Error)
}


class RequestManager {
    
    /// Função estática que retorna o JSON com os filmes e faz o parse de JSON to Movies
    ///
    /// - Parameter completion: closure que é executada após a requisição, em caso de sucesso é retornado os filmes, em caso de falha é retornado um erro.
    static func requestMovies(completion: @escaping (_ movies: Result) -> Void) {
        
        let URL = "https://sky-exercise.herokuapp.com/api/Movies"
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request(URL).responseArray { (response: DataResponse<[Movie]>) in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            switch response.result {
            case .success:
                guard let movies = response.result.value else {
                    completion(.success([]))
                    return
                }
                completion(.success(movies))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}

