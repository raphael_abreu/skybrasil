//
//  MovieCollectionViewCell.swift
//  SKY
//
//  Created by Raphael Abreu on 22/02/2018.
//  Copyright © 2018 Sky Brasil. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet var imgMovie: UIImageView!
    @IBOutlet var lblMovieTitle: UILabel!
    
    static let nib = UINib.init(nibName: "MovieCollectionViewCell", bundle: Bundle.main)
    static let identifier = "MovieCollectionViewCell"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    static func configureCell(collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, movieViewModel: MovieViewModel) -> MovieCollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.identifier, for: indexPath) as! MovieCollectionViewCell
        
        cell.lblMovieTitle.text = movieViewModel.title
        cell.imgMovie.af_setImage(withURL: movieViewModel.coverUrl!, placeholderImage: Movie.coverPlaceholder, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: true, completion: nil)
        cell.imgMovie.layer.cornerRadius = 5
        
        return cell
    }
}
