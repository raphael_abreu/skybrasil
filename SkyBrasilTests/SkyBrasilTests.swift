//
//  SkyBrasilTests.swift
//  SkyBrasilTests
//
//  Created by Raphael Abreu on 23/02/2018.
//  Copyright © 2018 Sky Brasil. All rights reserved.
//

import XCTest
@testable import SkyBrasil
import Nimble

class SkyBrasilTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    /// Testa a performace da requisição dos filmes
    func testPerformanceRequestMovies() {
        var returned: Bool = false
        
        RequestManager.requestMovies { (Result) in
            returned = true
        }
        
        expect(returned).toEventually(equal(true), timeout: 10, pollInterval: 0.5, description: "Espera que a chamada do serviço retorne dentro de 10 segundos")
    }
    
    
    /// Testa a requisição dos filmes e o parse JSON to Movies
    func testRequestMoviesSerialization() {
        var movieList: [Movie] = []
        
        RequestManager.requestMovies { (Result) in
            switch Result {
            case .success(let movies):
                movieList = movies
                break
            case .failure(_):
                break
            }
        }
        
        expect(movieList.count).toEventually(beGreaterThan(0), timeout: 10, pollInterval: 0.5, description: "Espera que a chamada do serviço retorne 12 filmes serializados")
    }
    
    
    /// Testa os títulos dos filmes que retornaram da requisição
    func testMoviesContainsMovieWithTitle() {
        let movieTitles: [String] = ["Doutor Estranho", "Kong: A Ilha da Caveira", "Capitão América: Guerra Civil", "Interestelar", "A Bela e a Fera", "Logan", "Estrelas Além do Tempo", "O Poderoso Chefinho", "De Volta ao Jogo", "Rogue One: A Star Wars Story", "Mulher-Maravilha ", "A Múmia"]
        
        var hasExpectedMovies: Bool = false
        
        RequestManager.requestMovies { (Result) in
            switch Result {
            case .success(let movies):
                for movie in movies {
                    if let title = movie.title, movieTitles.contains(title) {
                        hasExpectedMovies = true
                        continue
                    } else {
                        hasExpectedMovies = false
                        break
                    }
                }
                break
            case .failure(_):
                break
            }
        }
        
        expect(hasExpectedMovies).toEventually(equal(true), timeout: 10, pollInterval: 0.5, description: "Compara os títulos dos filmes retornados com os titulos esperados")
    }
}
