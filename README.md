# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Repositorio para hospedar desafio SKY Brasil
* Swift 4 (Xcode 9)
* CocoaPods para Gestão de dependências

### How do I get set up? ###

* Baixar o projeto da branch master
* Acessar a pasta raiz do projeto via terminal e rodar o comando "pod install" as dependencias serão instaladas
* Para executar os testes no Xcode pressionar CMD + U
* Para rodar o projeto no Xcode, selecionar o device a ser executado e pressionar CMD + R

### Who do I talk to? ###

* Raphael Abreu